
package com.coeding.java.baseball;

import java.util.ArrayList;
import java.util.List;

public class Team02 extends Player {
	static int getRand = 123;
	List<Integer> array;

	int[] answer3;
	int[] tam;
	int[] tam1;
	int[] tam2;
	int[] finalNum;

	@Override
	public void ready() {
		// have to set your answer
		super.ready();

		array = new ArrayList<Integer>();
		answer3 = new int[3];
		tam = new int[] { 0, 0, 0 };
		tam1 = new int[] { 0, 0, 0 };
		tam2 = new int[] { 0, 0, 0 };
		finalNum = new int[] { 0, 0, 0 };

	}

	// Khoi tao
	void khoitao() {
		for (int i = 4; i < 10; i++) {
			array.add(i);
		}
	}

	// hàm tách answer thành array 3 phần tử
	private int[] tachSo(int answer) {

		String temp = Integer.toString(answer);
		int[] newGuess = new int[temp.length()];
		for (int i = 0; i < temp.length(); i++) {
			newGuess[i] = temp.charAt(i) - '0';
		}

		return newGuess;
	}

	@Override
	public void predict(int strike, int ball) {
		khoitao();

// ===========================================================================================================
		// strike + ball == 0
		if (strike + ball == 0) {
			getRand = 0;
			for (int i = 0; i < 3; i++) {
				answer3[i] = array.get(0);
				array.remove(0);
				getRand = getRand * 10 + answer3[i];
			}
		}

// ===========================================================================================================
		// strike + ball == 1
		if (strike + ball == 1) {
			int[] guess = tachSo(getRand);
			// 1 ball 0 strike

			if (strike == 0 && ball == 1) {

				// đổi vị trí để tìm strike
				// so sánh giá trị trước với giá trị đang truyền vào

				if (tam1[2] == guess[2]) {
					tam2 = tam1;
					int b = tam1[0];
					tam1[0] = tam1[2];
					tam1[2] = b;
					for (int i = 0; i < 3; i++) {
						getRand = getRand * 10 + guess[i];
					} 
					
					
					
				} else if (tam2[1] == guess[1]) {
					answer3[0] = array.get(0);
					getRand = getRand * 10 + answer3[0];
					
					
					finalNum[1] = guess[1];
					answer3[1] = guess[1];
					getRand = getRand * 10 + answer3[1];
					
					answer3[2] = array.get(1);
					getRand = getRand * 10 + answer3[2];
					
					array.remove(1);
					array.remove(0);
					
					
				} else if (tam[0] == guess[0]) {
					int b = guess[0];
					guess[0] = guess[2];
					guess[2] = b;
					for (int i = 0; i < 3; i++) {
						tam[i] = guess[i];
					}
				} else {
					int b = guess[1];
					guess[1] = guess[2];
					guess[2] = b;
					for (int i = 0; i < 3; i++) {
						tam[i] = guess[i];
					}
				}

				for (int i = 0; i < 3; i++) {
					getRand = getRand * 10 + guess[i];
				}
			}

			// 1 strike 0 ball
			if (strike == 1 && ball == 0) {
				
				if(tam1[2] == guess[2]) {
					
					answer3[0] = array.get(0);
					getRand = getRand * 10 + answer3[0];
					
					
					
					answer3[1] = array.get(1);
					getRand = getRand * 10 + answer3[1];
					
					finalNum[2] = guess[2];
					answer3[2] = guess[2];
					getRand = getRand * 10 + answer3[2];
					
					array.remove(1);
					array.remove(0);
					
					
					
				}else {
				tam1 = guess;
				
				int b = guess[0];
				guess[0] = guess[1];
				guess[1] = b;
				for (int i = 0; i < 3; i++) {
					getRand = getRand * 10 + guess[i];
				}
				if(tam2[1] == guess[1] ){
					finalNum[0] = guess[0];
					
					answer3[0] =guess[0];
					getRand = getRand * 10 + answer3[0];
					
					
					
					answer3[1] = array.get(1);
					getRand = getRand * 10 + answer3[1];
					
					answer3[2] = array.get(2);
					getRand = getRand * 10 + answer3[2];
					
					array.remove(1);
					array.remove(0);
					
				}
				}
				
			}
		}

// ===========================================================================================================
		// strike + ball == 2
		if (strike + ball == 2) {
			int[] a = tachSo(getRand);
			// 0 strike 2 ball
			if (strike == 0 && ball == 2) {

				if (tam[0] == a[0]) {
					int b = a[0];
					a[0] = a[2];
					a[2] = b;
					for (int i = 0; i < 3; i++) {
						tam[i] = a[i];
					}
				} else {
					int b = a[1];
					a[1] = a[2];
					a[2] = b;
					for (int i = 0; i < 3; i++) {
						tam[i] = a[i];
					}
				}

				for (int i = 0; i < 3; i++) {
					getRand = getRand * 10 + a[i];
				}
			}

			// 1 strike 1 ball
			if (strike == 1 && ball == 1) {
		
				
			}

			// 2 strike 0 ball
			if (strike == 2 && ball == 0) {
			
			}
		}

// ===========================================================================================================
		// strike + ball == 3
		if (strike + ball == 3) {
			int[] a = tachSo(getRand);
			// strike = 0 , ball = 3
			if (strike == 0 && ball == 3) {

				int b = a[0];
				a[0] = a[1];
				a[1] = a[2];
				a[2] = b;

				for (int i = 0; i < 3; i++) {
					getRand = getRand * 10 + a[i];
				}
			}

			// strike = 1 , ball = 2

			if (strike == 1 && ball == 2) {
				int b = a[1];
				a[1] = a[2];
				a[2] = b;
				for (int i = 0; i < 3; i++) {
					getRand = getRand * 10 + a[i];
				}
			}

		}

	}

	// static int getRand = 123;
//	 List<Integer> array = new ArrayList<Integer>(); int[] answer3 = new int[3];
//	 
//	  
//	  void khoitao() { for (int i = 4; i < 10; i++) { array.add(i); } }
//	  
//	  @Override public void ready() { // TODO Auto-generated method stub
//	  super.ready(); }
//	  
//	  // hàm tách answer thành array 3 phần tử private int[] tachSo(int answer) {
//	  
//	  String temp = Integer.toString(answer); int[] newGuess = new
//	  int[temp.length()]; for (int i = 0; i < temp.length(); i++) { newGuess[i] =
//	  temp.charAt(i) - '0'; }
//	  
//	  return newGuess; }
//	  
//	  // hàm đoán số
//	  
//	  @Override public int call() {
//	  
//	  return getRand; }
//	  
//	  @Override public void predict(int strike, int ball) {
//	  
//	  if (strike == 0 && ball == 0) { getRand = 0; for (int i = 0; i < 3; i++) {
//	  answer3[i] = array.get(0); array.remove(0); getRand = getRand * 10 +
//	  answer3[i]; }
//	  
//	  
//	  // 0 strike 1 ball => 1strike 0 ball
//	if (strike == 0 && ball != 0) { int[] a
//	  = tachSo(getRand);
//	  
//	  if (tam[0] == a[0]) { int b = a[0]; a[0] = a[2]; a[2] = b; for (int i = 0; i
//	  < 3; i++) { tam[i] = a[i]; } } else { int b = a[1]; a[1] = a[2]; a[2] = b;
//	  for (int i = 0; i < 3; i++) { tam[i] = a[i]; } }
//	  
//	  for (int i = 0; i < 3; i++) { getRand = getRand * 10 + a[i]; } }
//	  
//	  if (strike == 1 && ball == 0) {
//	  
//	  }
//	  
//	  
//	  }
//	  
//	  
//	  0 strike 2 ball trường hợp 1 : 1 strike 1 ball trường hớp 2 : 2 strike 0 ball
//	  
//	 
//			if (strike == 0 && ball == 2) {
//				
//			}

//		}

	/*
	 * if (strike == 0 && ball == 0) { //answer tách thành 3 chữ số, có mảng từ 1 ~9
	 * remove 3 số trong mảng, random 1 số 3 chữ số trong mảng còn lại } if (strike
	 * == 0 && ball == 1) {//tach 3 chu so, swap
	 * 
	 * } if (strike == 0 && ball == 2) {
	 * 
	 * } if (strike == 0 && ball == 3) {
	 * 
	 * } if (strike == 1 && ball == 0) {
	 * 
	 * } if (strike == 1 && ball == 1) {
	 * 
	 * } if (strike == 1 && ball == 2) {
	 * 
	 * } if (strike == 2 && ball == 0) {
	 * 
	 * }
	 */

	/*
	 * public static void main(String[] args) { System.out.println(); }
	 */
}
