package com.coeding.java.baseball;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Random;

public class Team06 extends Player{
	protected int value;
	protected boolean flaga, flagb, flagc, flag;
	public int strike;
	public int ball;
	public int count, tempa, tempb, tempc;
	public Random rd;
	
	@Override
	public void ready() {
		value = genRandom();
		count = 0;
		tempa = tempb = tempc = 0;
		flag = false;
		flaga = false;
		flagb = false;
		flagc = false;
		answer = genRandom();
		rd = new Random();
	}
	
	@Override
	public int call() {
		return value;
	}
	
//	@Override
//	public void predict(int strike, int ball) {	
//		
//		int a = value / 100;
//		int b = (value / 10) % 10;
//		int c = value % 10;
//	
//		if (strike + ball != 3){
//			value = genRandom();
//		} else if (strike + ball == 3) {
//			if (flag == false) {
//				tempa = a;
//				tempb = b;
//				tempc = c;
//				flag = true;
//			}
//			if (count == 0) {
//				value = tempa*100 + tempc*10 + tempb;
//				count++;
//			} else if (count == 1) {
//				value = tempb*100 + tempa*10 + tempc;
//				count++;
//			} else if (count == 2) {
//				value = tempb*100 + tempc*10 + tempa;
//				count++;
//			} else if (count == 3) {
//				value = tempc*100 + tempa*10 + tempb;
//				count++;
//			} else {
//				value = tempc*100 + tempb*10 + tempa;
//			}
//		}
//		
//		System.out.println("나는 아무 생각이 없다 :"+answer);
//	}
	
	// cach 2
	@Override
	public void predict(int strike, int ball) {	
		
		int a = value / 100;
		int b = (value / 10) % 10;
		int c = value % 10;

		if (strike==0) {
			a++;
			while ( (a==b) || (a==c) || (b==c) ) {
				a++;
			}
			if (a==10) a=1;
			while ( (a==b) || (a==c) || (b==c) ) {
				a++;
			}
			value = a*100 + b*10 + c;
			flag = true;
		} else if (strike==1 && flag==true) {		
				b++;
				while ( (a==b) || (a==c) || (b==c) ) {
					b++;
				}
				if (b==10) b=1;
				while ( (a==b) || (a==c) || (b==c) ) {
					b++;
				}
				value = a*100 + b*10 + c;
		
		} else if (strike==2 && flag==true) {		
				c++;
				while ( (a==b) || (a==c) || (b==c) ) {
					c++;
				}
				if (c==10) c=1;
				while ( (a==b) || (a==c) || (b==c) ) {
					c++;
				}
				value = a*100 + b*10 + c;		
		} else {
			value = genRandom();
		}
		
		System.out.println("나는 아무 생각이 없다 :"+answer);
	}
	
	int genRandom() {
		boolean[] flag = new boolean[10];
		int number = 0;
		for(int i=0; i < 3; ++i) {
			int r;
			do {
				r = (int)(Math.random()*100)%9+1;
			}while( flag[r] );
			number = (number*10) + r;
			flag[r] = true;
		}
		return number;
	}
	
}
