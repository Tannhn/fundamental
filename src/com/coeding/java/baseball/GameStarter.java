package com.coeding.java.baseball;

import java.io.IOException;

/**
 * 
 * Java Fundamentals final test
 * 
 * 야구게임
 * - 3자리 정수를 서로 맞추는 예측 게임
 * - 값 같다, 위치 같다 스트라이크 : 스트라이크 3이면 정답을 맞춘 것
 * - 값 같다, 위치 다르다 볼
 * 
 * @author edupalm
 *
 */
public class GameStarter {

	public static void main(String[] args) throws IOException {
		Player[] robot = { 
				new Team01(), new Team02(), new Team03(), 
				new Team04(), new Team05(), new Team06(), new Tutor00()};
		int[] score = new int[robot.length];
		
		for(int p1=0; p1 < robot.length;p1++) {
			for(int p2=0; p2 < robot.length;p2++) {
				if( p1 != p2 ) {
					System.out.println((p1+1)+" vs "+(p2+1));
					deplay(100);
					robot[p1].ready();
					robot[p2].ready();
					int caller = p1;
					int replier = p2;
					int loop = 0;
					while( loop < 504 ) {
						loop++;
						int value = robot[caller].call();
						vaildate(value, robot[caller]);
						int[] count = robot[replier].reply(value);
						vaildate(count, robot[replier]);
						int strike = count[0];
						int ball = count[1];
						System.out.println((caller+1)+"["+value+"] -> "+(replier+1)+"[S: "+strike+", B: "+ball+"]");
						if(strike == 3 ) {
							System.out.println((caller+1)+" team win");
							deplay(200);
							score[caller]++;
							break;
						}
						robot[caller].predict(strike, ball);
						caller = ( caller == p1 ) ? p2 : p1;						
						replier = ( replier == p1 ) ? p2 : p1;						
					}
				}
			}			
		}
		
		for(int i=0; i < robot.length;i++) {
			System.out.println("team["+(i+1)+"]\t"+score[i]);
		}

	}

	private static void vaildate(int[] count, Player replier) {
		if( count.length != 2) {
			System.out.println(replier.getClass().getName());
			System.out.println(" must be 2 length[strike, ball] ");
			System.exit(-1);			
		}
	}

	private static void vaildate(int value, Player caller) {
		String sv = ""+value;
		if(sv.length() < 3 || sv.contains("0")) {
			System.out.println(caller.getClass().getName());
			System.out.println(" must be 3 digits and dont use '0' ");
			System.exit(-1);
		}
		int a = value / 100;
		int b = (value / 10) % 10;
		int c = value % 10;
		if( (a==b) || (a==c) || (b==c) ) {
			System.out.println(caller.getClass().getName());
			System.out.println("team wrong call value");
			System.exit(-1);
		}		
	}

	private static void deplay(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}