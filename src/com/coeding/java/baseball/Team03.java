package com.coeding.java.baseball;
// VG
public class Team03 extends Player {
	private int[] boSoChinhHop;
	private int seq;
	protected int saveAnswer;
	private int nullBall;
	private boolean checkBall;
	private static int[] checkarray = new int [9] ;
	
	public void ready() {
		answer = genRandom();
		boSoChinhHop = printCombinations(123456789);
		seq = 0;
		saveAnswer = -1;
		checkBall = false;
		for (int i=0; i<9 ; i++) {
		checkarray[i]=0;
		}
		nullBall=1;
		
	}
	
	private final int genRandom() {
		boolean[] flag = new boolean[10];
		int number = 0;
		for(int i=0; i < 3; ++i) {
			int r;
			do {
				r = (int)(Math.random()*100)%9+1;
			}while( flag[r] );
			number = (number*10) + r;
			flag[r] = true;
		}
		return number;
	}
	public int call() {
	//	int rs = boSoChinhHop[seq];
		if (nullBall != 0) {
			for (int i = seq; i < boSoChinhHop.length; i++) {
				if (tocheckarr(boSoChinhHop[i])) {
					seq =i;
					saveAnswer = boSoChinhHop[seq];
					seq++;
					return saveAnswer;
				}
			}
			
		}
//		saveAnswer = boSoChinhHop[seq];
		return saveAnswer;
	}
// return true neu ca 3 so deu khong nam trong day loai tru
	private boolean tocheckarr(int a) {
		int temp = a;
		int i;
		while (temp>0) {
			i = temp%10;
			if(checkarray[i-1]==1) {
				return false;
			}
			temp = temp/10;
		}
		return true;
	}

	public void predict(int strike, int ball) {
		int rs = strike + ball;
		if (rs == 3 && checkBall == false) {
			boSoChinhHop = hoanvi(saveAnswer);
			seq = 0;
			checkBall = true;
			System.out.println(checkBall);
		}
		if (rs == 0) {
			nullBall = saveAnswer;
			checkNullBall(nullBall);
		}
	}
	// *** check 0 ball
//	private static boolean checkNullBall(int a, int b) {
//		char[] charsA = ("" + a).toCharArray();
//		char[] charsB = ("" + b).toCharArray();
//		for (int i = 0; i < 3; i++) {
//			for (int j = 0; j < 3; j++) {
//				if (charsA[i] == charsB[j]) {
//					return false;
//				}
//			}
//		}
//		
//		return true;
//	}
	
	private static void checkNullBall(int a) {
		int temp = a;
		int i;
		while (temp>0) {
			i = temp%10;
			checkarray[i-1]=1;
			temp = temp/10;
		}
	}
	
	
	/**
	 * Config
	 */
	
	private static int[] hoanvi(int num) {
		int lotteryDigit1 = num / 100;
		int lotteryDigit2 = num / 10 % 10;
		int lotteryDigit3 = num % 10;
		int[] input = { lotteryDigit1, lotteryDigit2, lotteryDigit3 };
		int[] arr = new int[6];
		int seq = -1;
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				for (int z = 0; z < 3; z++) {

					if (x != y && y != z && z != x) {
						seq++;
						String k = input[x] + "" + input[y] + "" + input[z];
						int numtest = Integer.parseInt(k);
						arr[seq] = numtest;
					}
				}
			}
		}
		return arr;
	}
	// Chinh hop
	private static int[] printCombinations(int input)
    {
        char[] digits = Integer.toString(input).toCharArray();
        int[] list = new int[84];
        int seq =0;
        for (int i = 0; i < digits.length - 2; i++) {
            for (int j = i + 1; j < digits.length - 1; j++) {
                for (int k = j + 1; k < digits.length; k++) {
                	StringBuilder listNum = new StringBuilder(Character.toString(digits[i]) + Character.toString(digits[j]) + Character.toString(digits[k]));
                	int num = Integer.parseInt(listNum.toString());
                	list[seq] = num;
                	seq++;
                }
            }
        }
        return list;
    }
}