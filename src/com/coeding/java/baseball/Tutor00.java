package com.coeding.java.baseball;

public class Tutor00 extends Player {
	private int[] numbers;
	private int callIndex;
	@Override
	public void ready() {
		super.ready();
		callIndex = 0;
		numbers = new int[504];
		int i=0;
		for(int a=1;a<10;a++) {
			for(int b=1;b<10;b++) {
				for(int c=1;c<10;c++) {
					if( (a!=b) && (a!=c) && (b!=c) ) {
						numbers[i] = (a*100)+(b*10)+c;
						++i;
					}
				}				
			}
		}
	}

	@Override
	public int call() {
		if(callIndex >= numbers.length) {
			callIndex = 0;
			System.out.println("What?");
			System.exit(callIndex);
		}
		// max 504 times call
		// 123 124 125 126 127 ----- 987
		return numbers[callIndex++];
	}
	
}
