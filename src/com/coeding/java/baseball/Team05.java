package com.coeding.java.baseball;

import java.util.Random;

public class Team05 extends Player {
	private int[] list;

	private int rs;

	private int seq;

	private int check_3B;

	private int check_1T2B;
	
	public int[] getList() {
		return list;
	}

	public void setList(int[] list) {
		this.list = list;
	}

	@Override
	public void ready() {
		list = new int[]{1,2,3,4,5,6,7,8,9};
		seq = list.length;
		answer = getRandom();
	}
	
	private int getRandom() {
		
		boolean[] flag = new boolean[10];
		int number = 0;
		for (int i = 0; i < 3; ++i) {
			int r;
			do {
				Random r1 = new Random();
				int idx = r1.nextInt(seq); //(0->n-1)
				r = list[idx];
			} while (flag[r]);
			number = (number * 10) + r;
			flag[r] = true;
		}
		return number;
	}
	

	@Override
	public int call() {
		if (rs == 0) {
			rs = getRandom();
		}
		return rs;
	}

	@Override
	public void predict(int strike, int ball) {
		System.out.println("Answer after handle of team A :" + answer);
		int a = rs / 100;
		int b = (rs / 10) % 10;
		int c = rs % 10;
		
		if (strike == 0 && ball == 0) {
			if(binary_search(a)!=-1) {
				delete(binary_search(a));
			}
			if(binary_search(b)!=-1) {
				delete(binary_search(b));
			}
			if(binary_search(c)!=-1) {
				delete(binary_search(c));
			}
			
			
			
			rs = getRandom();
		}
		if (strike == 1 && ball == 2) {
			final int temp2 = rs;
			int x = temp2 / 100;
			int y = (temp2 / 10) % 10;
			int z = temp2 % 10;
			check_1T2B++;
			switch (check_1T2B) {
			case 1:
				rs = y * 100 + x * 10 + b;
				break;
			case 2:
				rs = x * 100 + z * 10 + y;
				break;
			case 3:
				rs = z * 100 + y * 10 + x;
				break;
			}
		}
		if (check_1T2B > 0) {
			check_1T2B = 0;
		}
		if (ball == 3) {
			final int temp1 = rs;
			int x = temp1 / 100;
			int y = (temp1 / 10) % 10;
			int z = temp1 % 10;
			check_3B++;
			switch (check_3B) {
			case 1:
				rs = y * 100 + z * 10 + x;
				break;
			case 2:
				rs = z * 100 + x * 10 + y;
				break;
			}
		}
		if (check_3B > 0) {
			check_3B = 0;
		}
		
		rs = getRandom();
		
	}

	private void delete(int delIndex) {
		for(int i=delIndex; i<seq-1; i++) {
			getList()[i]=getList()[i+1];
		}
		--seq;	
	
	}

//
//	private int random() {
//		Random rd = new Random();
//		int i = rd.nextInt(list.length - 1); // (0->listsize-1)
//		return list[i];
//	}

//	private int search(int x) {
//		for(int i=0; i<list.length; i++) {
//			if(list[i]==x) {
//				return i;
//			}
//		}
//		return -1;
//	}
//	
	private int binary_search(int x) {
		int low = 0;
		int high = list.length - 1;
		int mid = 0;
		while (low <= high) {
			mid = (high + low);
			if (list[mid] < x) {
				low = mid + 1;
			} else if (list[mid] > x) {
				high = mid - 1;
			} else {
				return mid;
			}
		}
		return -1;
	}


}
