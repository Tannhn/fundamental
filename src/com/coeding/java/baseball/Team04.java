package com.coeding.java.baseball;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Team04 extends Player {

	private int callingNumber;

	private Set<Integer> result;
	private List<String> listSplitNumber;
	private List<String> listNumber ; 

	public Team04() {
		// just one called constructor
	}

	@Override
	public void ready() {
		// each game
		super.ready();// have to set your answer
		
		result = new HashSet<Integer>();
		listSplitNumber = new ArrayList<String>();
		listNumber = new ArrayList<String>();
		//initialize listNumber from 1 ->9
		innitializeListNumber();
		System.out.println("before: "+listNumber);//sysout testing
		callingNumber = super.call();
	}

	@Override
	public int call() {
		System.out.println("calling number: " + callingNumber);//sysout testing
		return callingNumber;
	}

	@Override
	public void predict(int strike, int ball) {
		//checking strike ball to predict
		if (strike + ball == 0) {
			if (removeNumber()) {
				randomNumberAfterRemove();
			}else {
				System.out.println("Remove Number Error!");
			}
					
		System.out.println(listNumber);//sysout testing
		} else if (strike + ball > 1) {

		}

	}

	//random number after remove number with strike + ball = 0
	public void randomNumberAfterRemove() {
		String mergeNumber ="";
		for (int i = 0; i < 3; i++) {
			int randomIndex = new Random().nextInt(listNumber.size());
		
			mergeNumber += listNumber.get(i);
		}
		int number = Integer.parseInt(mergeNumber);
		
		if (vaildateUniqueNumber(number)) {
			callingNumber = number;
			
		}else {
			randomNumberAfterRemove();
		}
	}
	
	//checking 3-digits number is unique?
	private boolean vaildateUniqueNumber(int value) {
		int a = value / 100;
		int b = (value / 10) % 10;
		int c = value % 10;
		if( (a==b) || (a==c) || (b==c) ) {
			return false;
		}		
		return true;
	}
	

	//removing number with strike + ball = 0
	public boolean removeNumber() {
		splitNumber(callingNumber);
		if (!listSplitNumber.isEmpty()) {
			for (int i = 0; i < listSplitNumber.size(); i++) {
				int listNumberIndex = hasDuplicate(listNumber, i);
				listNumber.remove(listNumberIndex);
			}
			listSplitNumber.clear();
			return true;
		}
			return false;
		
	}
	
	//return index of listNumber to remove in list
	public int hasDuplicate(List<String> list, int index) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).equals(listSplitNumber.get(index))) {
				return i;
			}
		}
		return -1;
	}
	
	//split 3-digits number into String
	public void splitNumber(int number) {
		String strNumber = String.valueOf(number);
		for (int i = 0; i < strNumber.length(); i++) {
			listSplitNumber.add(String.valueOf(strNumber.charAt(i)));
		}
		System.out.println("split number: "+listSplitNumber);//sysout testing
	}
	
	
	public void innitializeListNumber() {
		for (int i = 1; i <=9; i++) {
			listNumber.add(i+"");
		}
	}

}
