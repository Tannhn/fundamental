package com.coeding.java.baseball;

public class Player {
	protected int answer;// child can access directly
	
	public void ready() {
		// each game start
		answer = genRandom();
	}
	// final method can't be override
	public final int[] reply(int value) {
		int strike = 0;
		int ball = 0;
		String sanswer = ""+ answer;
		if( (answer+"").length() != 3 ) {
			System.out.println(this.getClass().getName()+" must be 3 digits answer");
			System.exit(0);
		}
		String svalue = ""+ value;
		for(int p1=0; p1<3; p1++) {
			char v1 = sanswer.charAt(p1);
			for(int p2=0; p2<3; p2++) {
				char v2 = svalue.charAt(p2);
				if( v1 == v2 ) {
					if( p1 == p2 ) {
						strike++;
					}else {
						ball++;
					}
				}				
			}
		}
		return new int[]{strike,ball};		
	}
	
	private final int genRandom() {
		// make 3digits number by random
		boolean[] flag = new boolean[10];
		int number = 0;
		for(int i=0; i < 3; ++i) {
			int r;
			do {
				r = (int)(Math.random()*10000)%9+1;
			}while( flag[r] );
			number = (number*10) + r;
			flag[r] = true;
		}
		return number;
	}

	public int call() {
		// send value to oppo
		return genRandom();
	}

	public void predict(int strike, int ball) {
		// 내가 좀 전에 123 을 call()ed
		//	strike 0, ball 2 라고 대답했네
		// 	상대방의 정답은 얼마일까? 
		System.out.println("나는 아무 생각이 없다 :"+answer);
	}

}
