package com.coeding.java.oop;

// Inheritance(상속)
// easy to change Type, legacy code re-use
// TV class field, method copied
// add field, method need 
public class VinTV extends TV{
	private OppoSpeaker speaker;
	@Override
	public void powerOn() {
		// TODO super means instance of TV
		System.out.println("VinTV brand TV on");
	}

	@Override
	public void volumnUp() {// 소리 커지는 기능은 실행되어야 한다.
		// if want use speaker of parent
		//	1. protected
		//	2. getSpeaker in parent
		speaker.soundUp();
		// if not use  speaker of parent
		// make	my speaker
	}

	// this method have no at TV class
	public void internet() {
		System.out.println("Connecting");
	}
	
}
