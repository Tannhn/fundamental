package com.coeding.java.oop;

// has abstract method
// 구현(implement) - 만들어서 실행할 수 있는 상태
// developer A : soundDown
public abstract class Speaker {
	public abstract void soundUp();// other developer
	// implemented method
	public void soundDown() {
		System.out.println("down -- -- ");
	}
}
