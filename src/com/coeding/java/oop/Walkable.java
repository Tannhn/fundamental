package com.coeding.java.oop;

// interface not class, cant create instance
// all method be abstract
// to use, have to implement class
// field must be final and static which can't changed
public interface Walkable {
	String version = "0.0.1-SNAPSHOT";
	public void walk();
	public void stop();

}
