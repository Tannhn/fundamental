package com.coeding.java.oop;

/*
 * my class
 * 	has :	fields
 * 	have to do  :	method
 */
public class TV {
	private OppoSpeaker speaker;// not recommended

	public TV() {
		speaker = new OppoSpeaker();
		System.out.println("create TV instance");
	}
	public void powerOn() {
		System.out.println("tv On");
	}
	public void powerOff() {
		System.out.println("tv Off");		
	}
	public void volumnUp() {
		speaker.soundUp();
	}
	public void volumnDown() {
		speaker.soundDown();		
	}

}
