package com.coeding.java.oop;

// not extends
public class WalkableImpl implements Walkable {

	@Override
	public void walk() {
		System.out.println("go walk");
	}
	
	// annotation @Override means already has 
	@Override
	public void stop() {
		System.out.println("stop walk");
	}

}
