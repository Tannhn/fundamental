package com.coeding.java.oop;

// as Printer
public class InkjetPrinter extends Printer {

	@Override
	public void print() {
		System.out.println("our printer to print by ink paint");
	}

}
