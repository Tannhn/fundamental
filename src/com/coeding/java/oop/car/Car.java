package com.coeding.java.oop.car;

// follow rule of Movable
public abstract class Car implements Movable {
	// all Car has Tire 
	private Tire[] front;// [0]left, [1]right
	private Tire[] rear;

	public Car() {
		front = new Tire[2];// assemble tire
		rear = new Tire[2];		
		System.out.println("Car has Tire 4 ");
	}
	
	@Override
	public void go() {
		System.out.println("move forward");		
	}

	@Override
	public void back() {
		System.out.println("move backward");				
	}
	
	// turnLeft, turnRight make by other developer
	
}
