package com.coeding.java.oop.car;

public class VinfastCar extends Car {
	// go, back method implemented already
	@Override
	public void turnLeft(double deg) {
		System.out.println("front tire rotate left");
	}

	@Override
	public void turnRight(double deg) {
		System.out.println("front tire rotate right");
	}

}
