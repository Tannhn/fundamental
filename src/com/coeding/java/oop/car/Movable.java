package com.coeding.java.oop.car;

// define rule of method
//	many developer coding one project together
//	method's name,parameter,return 
//	인터페이스(interface) : 메서드(method)의 규칙 정하기
//	추상클래스(class) : 일부만 완성된 클래스(공통된 기능만 먼저 만든 것)
//	구현클래스가 사용(new ()) 할 수 있다.
//
public interface Movable {
	public void go();
	public void back();
	public void turnLeft(double deg);
	public void turnRight(double deg);
}
