package com.coeding.java.oop;

public abstract class Printer {

	// common function any printer
	public void connect() {
		System.out.println("USB cable connected");
	}
	public abstract void print();
	// how to print ? 종이에 인쇄하는 방법은 다르다

}
