package com.coeding.java.blog;

// method' rule like design for project
public interface ArticleDAO {
	//what data type? 
	public void insert(Article vo);
	public void update(Article vo);
	public void delete(int id);
	public Article[] select();// all articles
	public Article[] selectBy(Article vo);
	public Article selectOne(String key);

}
