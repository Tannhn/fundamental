package com.coeding.java.blog;

/**
 * 
 * Model role
 * DAO : Data Access Object
 * implement CRUD for Article
 * 
 * @author Administrator
 *
 */
public class ArticleDAOImpl implements ArticleDAO {
	private Article[] list;
	private int seq;
	// default constructor
	public ArticleDAOImpl() {
		list = new Article[10];// length fixed 10		
	}

	public boolean save(Article article) {
		boolean result = false;
		if( seq < list.length ) {
			list[ seq ] = article;	
			++seq;
			result = true;
		}
		return result;
	}

	public Article getArticle(int idx) {
		Article origin = list[idx];
		// is Exist ?
		if( origin != null ) {
			// origin -> rt
			Article rt = new Article(origin);
			return rt; // copied instance
		}
		return null;
	}

	public void update(Article article, int idx) {
		list[idx] = article;
	}

	public Article[] searchArticleByWriter(String writer) {
		// 1. count article which  has writer
		int count = 0;
		for(int i=0; i<list.length;i++) {
			// use Reference type Array
			if( list[i] != null ) {
				if( list[i].getWriterName().equals(writer) ) {
					count+=1;
				}
			}
		}
		if( count > 0 ) {
			// has writer
			Article[] result = new Article[count];
			int k = 0; // index of result
			for(int i=0; i<list.length;i++) {
				if( list[i] != null ) {
					if( list[i].getWriterName().equals(writer) ) {
						// copied instance
						Article rt = new Article(list[i]);
						result[k] = rt;
						k++;
					}
				}
			}
			return result;
		}
		return null;
	}

	public Article[] getAll() {
		// private field "list"
			// can't be change by other class
		// broken means of private
		System.out.println(list+" in dao");
		return list.clone();// method of Array
	}

	@Override
	public void insert(Article vo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Article vo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Article[] select() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Article[] selectBy(Article vo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Article selectOne(String key) {
		// TODO Auto-generated method stub
		return null;
	}
}












