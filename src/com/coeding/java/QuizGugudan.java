package com.coeding.java;

public class QuizGugudan {

	public static void main(String[] args) {
//		Đánh giá cấp độ cá nhân
//		In bảng cửu chương
		gugudan(20);// 2 ~ 9
	}
	private static void gugudan(int cols) {
		// what change, how change, has pattern <--- programming sense
		for(int dan=2; dan<10; dan+=cols) {
			for(int v=1; v<10; v+=1) {
				for(int i=0; i<cols; i+=1) {
					// i dan+0  dan+1 	dan+2 -> 5 6 7 when dan = 5
					if( (dan+i) <= 9 ) {
						int mul = (dan+i) * v;
						System.out.print((dan+i)+" x "+v+" = "+mul+"\t");
					}
				}
				System.out.println();// line feed
			}
		}

	}

	private static void solved(int cols) {
		// 화면에 출력하는 규칙을 알아내는 것
		for(int base=2; base<10; base+=cols) {
			// base : left start value
			for(int v=1;v<=9;v++) {
				for(int c=0;c<cols;c++) {
					// c : 0 1 2 <- cols : 3
					// base : 2
					int dan = base + c;
					int multi = dan * v;
					if( dan < 10 ) {
						System.out.print(dan+" x "+v+" = "+multi+"\t");
						// dan : 2 3 4 5 6 7 8 9
					}
				}
				// v : 1 2 3 4 5 6 7 8 9
				System.out.println();
			}
		}		
	}
	
	

}
